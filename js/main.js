var batLoading = function () {
  document.getElementById("loading").style.display = "flex";
};
var tatLoading = function () {
  document.getElementById("loading").style.display = "none";
};
var fetchDssvService = function () {
  batLoading();
  axios({
    url: `https://633ec05c0dbc3309f3bc546d.mockapi.io/sv`,
    method: "GET",
  })
    .then(function (res) {
      console.log("res: ", res);
      dssv = res.data;
      renderDanhSachSinhVien(res.data);
      tatLoading();
    })
    .catch(function (err) {
      console.log("err: ", err);
      tatLoading();
    });
};
fetchDssvService();
var renderDanhSachSinhVien = function (list) {
  var contentHTML = "";
  list.forEach(function (sv) {
    contentHTML += `<tr>
        <td>${sv.ma}</td>
        <td>${sv.ten}</td>
        <td>${sv.email}</td>
        <td>0</td>
        <td>
        <button onclick="layThongTinChiTietSv(${sv.ma})" class="btn btn-primary">Sửa</button>
        <button onclick="xoaSv(${sv.ma})" class="btn btn-danger">Xoá</button>
        </td>
      </tr>`;
  });
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

function xoaSv(idSV) {
  batLoading();
  axios({
    url: `https://633ec05c0dbc3309f3bc546d.mockapi.io/sv/${idSV}`,
    method: "DELETE",
  })
    .then(function (res) {
      fetchDssvService();
      Swal.fire("Xóa thành công");
      tatLoading();
    })
    .catch(function (err) {
      Swal.fire("Xóa thất bại");
      tatLoading();
    });
}

function themSV() {
  var sv = layThongTinTuForm();
  axios({
    url: `https://633ec05c0dbc3309f3bc546d.mockapi.io/sv`,
    method: "POST",
    data: sv,
  })
    .then(function (res) {
      fetchDssvService();
      Swal.fire("Thêm thành công");
    })
    .catch(function (err) {
      Swal.fire("Thêm thất bại");
    });
}

var layThongTinChiTietSv = function (idSV) {
  axios({
    url: `https://633ec05c0dbc3309f3bc546d.mockapi.io/sv/${idSV}`,
    method: "GET",
  })
    .then(function (response) {
      showThongTinLenForm(response.data);
    })
    .catch(function (error) {
      console.log("error: ", error);
    });
};

var capNhatSv = function () {
  var sv = showThongTinLenForm();
  axios({
    url: `https://633ec05c0dbc3309f3bc546d.mockapi.io/sv/${sv.ma}`,
    method: "PUT",
    data: sv,
  });
};
